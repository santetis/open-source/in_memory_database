import 'dart:async';

import 'package:database_interface/database_interface.dart';
import 'package:in_memory_database/src/collection.dart';

class InMemoryDatabase implements NoSqlDatabaseInterface {
  static InMemoryDatabase _inMemoryDb;

  final _db = <String, InMemoryCollection>{};

  bool _isOpen = false;

  factory InMemoryDatabase() => _inMemoryDb ??= InMemoryDatabase._();

  InMemoryDatabase._();

  @override
  bool get isOpen => _isOpen;

  @override
  Future<void> close() async {
    _isOpen = false;
  }

  @override
  InMemoryCollection collection(String collectionName) {
    if (!_isOpen) {
      throw DatabaseException('Need to open the database first');
    }
    return _db[collectionName] ??= InMemoryCollection();
  }

  @override
  Future<void> open() async {
    _isOpen = true;
  }
}
